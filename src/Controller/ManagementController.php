<?php
declare(strict_types=1);

namespace App\Controller;

class ManagementController extends AppController
{
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->loadComponent('Authentication.Authentication');
        $this->viewBuilder()->setLayout('admin'); 
        $this->processAdmin();
    }
    
    public function dashboard(){
        
    }
}
