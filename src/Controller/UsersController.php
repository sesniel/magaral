<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;

class UsersController extends AppController
{
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->loadComponent('Authentication.Authentication');
        $this->Authentication->addUnauthenticatedActions(['login', 'logout']);
        $this->viewBuilder()->setLayout('admin'); 
    }
    
    public function login()
    {
        $this->viewBuilder()->setLayout('login'); 
        $this->request->allowMethod(['get', 'post']);

        // display error if user submitted and authentication failed
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            if (filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $result = $this->Authentication->getResult();
                if ($result->isValid()) {
                    
                    $userDetails = $result->getData('role');
                    
                    if($userDetails->role == 'Admin'){
                        return $this->redirect('admin/dashboard');
                    }else if($userDetails->role == 'Teacher'){
                        return $this->redirect('teacher/dashboard');
                    }
                } else {
                    $this->Flash->error(__('Invalid username or password'));
                }
              } else {

                $data = $this->request->getData();

                list($studentNumber, $tmpBirthDate) = explode('_', $data['email']);
                $birthDate = substr($tmpBirthDate, 0, 4) .'-'. substr($tmpBirthDate, 4,2) .'-'. substr($tmpBirthDate,6,2);

                $password = Security::hash($data['password']);
                pr($this->request->getData());
                // echo $password . "<br/>";

                $query = TableRegistry::get('Accounts')->find('all', [
                    'conditions' => ['Accounts.student_number' => $studentNumber,
                                    'Accounts.birthdate' => $birthDate,
                                    'Accounts.password' => $password]
                ])->select(['id', 'student_number', 'first_name', 'last_name', 'token']);
                $data = $query->first();

                if ( !empty($data) ) {
                    // Query or result set is empty
                // pr($data);
                $_SESSION['Auth'] = $data;
                    return $this->redirect('student/dashboard');
                // pr($_SESSION['Auth']);
                }else{
                    echo $studentNumber . " <br/> " . $birthDate;exit;
                }

              }
        }
        
        
    }

    public function logout()
    {
        $result = $this->Authentication->getResult();
        // regardless of POST or GET, redirect if user is logged in
        // if ($result->isValid()) {
            $this->Authentication->logout();
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        // }
    }
    
}
