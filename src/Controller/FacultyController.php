<?php
declare(strict_types=1);

namespace App\Controller;

class FacultyController extends AppController
{
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->loadComponent('Authentication.Authentication');
        $this->viewBuilder()->setLayout('faculty'); 
        $this->processFaculty();
    }
    
    public function dashboard(){
        
    }
}
