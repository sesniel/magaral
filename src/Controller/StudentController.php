<?php
declare(strict_types=1);

namespace App\Controller;

class StudentController extends AppController
{
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->viewBuilder()->setLayout('student'); 
        $this->processStudent();
    }
    
    public function dashboard(){
        
    }
}
