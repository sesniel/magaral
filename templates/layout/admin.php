<?php

    $cakeDescription = 'Student Management System';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <?= $this->Html->css('assets/css/icons.css') ?>
    <?= $this->Html->css('assets/plugins/owl-carousel/owl.carousel.css') ?>
    <?= $this->Html->css('assets/plugins/mscrollbar/jquery.mCustomScrollbar.css') ?>
    <?= $this->Html->css('assets/plugins/sidebar/sidebar.css') ?>
    <?= $this->Html->css('assets/css/sidemenu.css') ?>
    <?= $this->Html->css('assets/plugins/jqvmap/jqvmap.min.css') ?>
    <?= $this->Html->css('assets/css/style.css') ?>
    <?= $this->Html->css('assets/css/style-dark.css') ?>
    <?= $this->Html->css('assets/css/skin-modes.css') ?>

</head>
<body>
    <div class="page">
        <?php 
        echo $this->element('admin_sidebar');
        echo $this->element('admin_header');
        ?>

        </div>
    </div>
		<a href="#top" id="back-to-top"><i class="las la-angle-double-up"></i></a>
        


    <?= $this->Html->script('assets/plugins/jquery/jquery.min'); ?>
    <?= $this->Html->script('assets/plugins/bootstrap/js/bootstrap.bundle.min'); ?>
    <?= $this->Html->script('assets/plugins/chart.js/Chart.bundle.min'); ?>
    <?= $this->Html->script('assets/plugins/ionicons/ionicons'); ?>
    <?= $this->Html->script('assets/plugins/moment/moment'); ?>
    <?= $this->Html->script('assets/plugins/jquery-sparkline/jquery.sparkline.min'); ?>
    <?= $this->Html->script('assets/plugins/raphael/raphael.min'); ?>
    <?= $this->Html->script('assets/plugins/jquery.flot/jquery.flot'); ?>
    <?= $this->Html->script('assets/plugins/jquery.flot/jquery.flot.pie'); ?>
    <?= $this->Html->script('assets/plugins/jquery.flot/jquery.flot.resize'); ?>
    <?= $this->Html->script('assets/plugins/jquery.flot/jquery.flot.categories'); ?>
    <?= $this->Html->script('assets/js/dashboard.sampledata'); ?>
    <?= $this->Html->script('assets/js/chart.flot.sampledata'); ?>
    <?= $this->Html->script('assets/plugins/mscrollbar/jquery.mCustomScrollbar.concat.min'); ?>
    <?= $this->Html->script('assets/js/apexcharts'); ?>
    <?= $this->Html->script('assets/plugins/rating/jquery.rating-stars'); ?>
    <?= $this->Html->script('assets/plugins/rating/jquery.barrating'); ?>
    <?= $this->Html->script('assets/plugins/perfect-scrollbar/perfect-scrollbar.min'); ?>
    <?= $this->Html->script('assets/plugins/perfect-scrollbar/p-scroll'); ?>
    <?= $this->Html->script('assets/js/eva-icons.min'); ?>
    <?= $this->Html->script('assets/plugins/sidebar/sidebar'); ?>
    <?= $this->Html->script('assets/plugins/sidebar/sidebar-custom'); ?>
    <?= $this->Html->script('assets/js/sticky'); ?>
    <?= $this->Html->script('assets/js/modal-popup'); ?>
    <?= $this->Html->script('assets/plugins/side-menu/sidemenu'); ?>
    <?= $this->Html->script('assets/plugins/jqvmap/jquery.vmap.min'); ?>
    <?= $this->Html->script('assets/plugins/jqvmap/maps/jquery.vmap.usa'); ?>
    <?= $this->Html->script('assets/js/index'); ?>
    <?= $this->Html->script('assets/js/apexcharts'); ?>
    <?= $this->Html->script('assets/js/custom'); ?>
    <?= $this->Html->script('assets/js/jquery.vmap.sampledata'); ?>
</body>
</html>
