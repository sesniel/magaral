<?php

    $cakeDescription = 'Student Management System';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    
    <?= $this->Html->css('assets/css/style.css') ?>
    <?= $this->Html->css('assets/css/skin-modes.css') ?>



</head>
<body class="main-body bg-light">
    <div class="page">
        <?php 
        echo $this->fetch('content');
        ?>

        </div>
    </div>
        


    <?= $this->Html->script('assets/js/custom'); ?>
</body>
</html>
