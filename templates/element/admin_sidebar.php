<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar sidebar-scroll">
    <div class="main-sidebar-header active">
        <h4>Admin Page</h4>
    </div>
    <div class="main-sidemenu">
        <div class="app-sidebar__user clearfix">
            <div class="dropdown user-pro-body">
                <div class="">
                    <?= $this->Html->image('faces/6.jpg', ['alt' => 'user-img', 'class' => "avatar avatar-xl brround"]); ?>
                    <span class="avatar-status profile-status bg-green"></span>
                </div>
                <div class="user-info">
                    <h4 class="font-weight-semibold mt-3 mb-0"><?= $_SESSION['Auth']['first_name'] ?></h4>
                    <span class="mb-0 text-muted"><?= $_SESSION['Auth']['role'] ?></span>
                </div>
            </div>
        </div>
        <ul class="side-menu">
            <li class="side-item side-item-category">Main</li>
            <li class="slide">
                <a class="side-menu__item" href="index.html">
                    <i class="fa fa-user-circle" aria-hidden="true"></i>
                    <span class="side-menu__label"> &nbsp;&nbsp; Advisee</span><span class="badge badge-primary side-badge">25</span>
                </a>
            </li>
            <li class="slide">
                <a class="side-menu__item" data-toggle="slide" href="#">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    <span class="side-menu__label"> &nbsp;&nbsp; Students</span><i class="angle fe fe-chevron-down"></i></a>
                <ul class="slide-menu">
                    <li><a class="slide-item" href="chart-morris.html">Attendance</a></li>
                    <li><a class="slide-item" href="chart-flot.html">Records</a></li>
                    <li><a class="slide-item" href="chart-chartjs.html">Subjects</a></li>
                </ul>
            </li>
            <li class="slide">
                <a class="side-menu__item" data-toggle="slide" href="#">
                    <i class="fa fa-book" aria-hidden="true"></i>
                    <span class="side-menu__label"> &nbsp;&nbsp; Subjects</span><i class="angle fe fe-chevron-down"></i></a>
                <ul class="slide-menu">
                    <li><a class="slide-item" href="chart-morris.html">English</a></li>
                    <li><a class="slide-item" href="chart-flot.html">Math</a></li>
                    <li><a class="slide-item" href="chart-chartjs.html">Physical Education</a></li>
                </ul>
            </li>
            <li class="slide">
                <a class="side-menu__item" href="index.html">
                    <i class="fa fa-folder-open" aria-hidden="true"></i>
                    <span class="side-menu__label"> &nbsp;&nbsp; Schedule</span>
                </a>
            </li>
            <!-- For Admin Only -->
            <li class="slide">
                <a class="side-menu__item" href="index.html">
                    <i class="fa fa-child" aria-hidden="true"></i>
                    <span class="side-menu__label"> &nbsp;&nbsp; Faculty</span><span class="badge badge-primary side-badge">10</span>
                </a>
            </li>
            <li class="slide">
                <a class="side-menu__item" href="index.html">
                    <i class="fa fa-window-restore" aria-hidden="true"></i>
                    <span class="side-menu__label"> &nbsp;&nbsp; Enrolment</span>
                </a>
            </li>


        </ul>
    </div>
</aside>