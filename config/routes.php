<?php

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;

$routes->setRouteClass(DashedRoute::class);

$routes->scope('/', function (RouteBuilder $builder) {
    // Register scoped middleware for in scopes.
    $builder->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true,
    ]));
    // $builder->applyMiddleware('csrf');


    $builder->connect('/login', ['controller' => 'Users', 'action' => 'login']);
    $builder->connect('/logout', ['controller' => 'Users', 'action' => 'logout']);

    // $builder->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);

    // $builder->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    $builder->fallbacks();
});

$routes->scope('/admin', function ($routes) { 
    // Register scoped middleware for in scopes.
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true,
    ]));
    $routes->connect('/dashboard', ['controller' => 'Management', 'action' => 'dashboard']);
    // $routes->connect('/', ['controller' => 'Users', 'action' => 'login']);

    $routes->fallbacks();
});

$routes->scope('/teacher', function ($routes) { 
    // Register scoped middleware for in scopes.
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true,
    ]));
    $routes->connect('/dashboard', ['controller' => 'Faculty', 'action' => 'dashboard']);
    // $routes->connect('/', ['controller' => 'Users', 'action' => 'login']);

    $routes->fallbacks();
});

